# Praktikum Jaringan Komputer Semester 3


1. Sulthan Mahendra Mubarak / 4210181019
2. Dicky Dwi Darmawan / 4210181028

This Program will slide the letter the client send to the 4th letter after it.

**How it works :**
1. Client will send the text to be converted
2. Server will receive the text sent by client.
3. The text that is received will be converted to array of char.
4. Server will checked for index of the array and changing its ASCII value
    *  If it's lowercase, the server will change it to the 4th letter after it.
    *  If it's uppercase, the server will change it to lowercase, and then change it to the 4th letter after it.
    *  If the letter is "W, X, Y, Z", it will return back to the start of the alphabet.
    *  If its [space], the program will not change the ASCII value.
5. If the client doesnt input any text when they press enter, the program will close.
